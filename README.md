
# CI&CD Pipeline implementation

- First we need to create jenkins pipelinejob.

![Logo](https://i.ibb.co/CbY3k0v/Capture.png)

- then tick enable github 

![Logo](https://i.ibb.co/cvPK9bP/Capture.png)

- copy below pipeline to jenkins 
```bash
  pipeline {
    agent any
    // declare varaible
    environment {
        chatID = "767718288"
        botToken =  "bot5371006076:AAFtkwih1-l5lmlBjmg2V1hCPyPGFkqI55Q"
        linkGit = "https://github.com/apd-developers/staff_management_microservice.git"
    }
    stages {
         stage("notification") {
            steps {
                // curl telegram api for post messeage
                sh "curl https://api.telegram.org/${env.botToken}/sendMessage?chat_id=${env.chatID} -d text='----jenkins trigger----' && curl https://api.telegram.org/${env.botToken}/sendMessage?chat_id=${env.chatID} -d text='---> check your job console here : https://jenkins.malinka.dev/job/staff_management_microservice/${currentBuild.number}/console'"
            }
        }
        stage("checkout project") {
            steps {
                // clone project
                 git url: "${env.linkGit}" , credentialsId: "microservice"
            }
        }
          stage("run integration test") {
            steps {
                // run integration test
                withGradle {
                    sh 'sudo chmod +x gradlew && ./gradlew test --tests IntegrationTest && ./gradlew test --tests HelloControllerTest && ./gradlew test --tests GreetingServiceTest'
                }
                    sh 'echo "integration test successfully.."'
            }
        }
        stage("build project") {
            steps {
                // build project
                withGradle {
                    sh 'sudo chmod +x gradlew && ./gradlew build'
                }
                sh 'echo "project build successfully..."'
            }
        }
        stage("deploy staging") {
            steps {
                // build docker images,push,run staging container (port 17000)
            sh '''
            fun_Dockerfile(){
                    echo "FROM adoptopenjdk/openjdk11
                    RUN mkdir -p workspace
                    COPY build/libs/*.jar /workspace
                    EXPOSE 8080"
                    echo 'ENTRYPOINT ["java","-jar","/workspace/staff_management_microservice-0.0.1-SNAPSHOT.jar"]'
 }
            fun_Dockerfile > Dockerfile
                    echo $(docker build -t rathjr/staff_management_microservice_staging .) > imagesLog_stag.txt
                    VR=$(grep 'Step 4'  imagesLog_stag.txt)
            if [ "$VR" == "" ]
            then
                    echo "images build failed"
            else
                    echo "build imaegs success"
                    docker login -u rathjr -p Rathkhmer2021
                    docker push rathjr/staff_management_microservice_staging
                    docker rm -f staff_management_microservice_staging
                    docker run -d --name staff_management_microservice_staging -p 17000:8080 rathjr/staff_management_microservice_staging
                    echo "done deploy stagging"
            fi
            '''
            }
        }
        stage("run regression test") {
            steps {
                 echo "regression.."
            }
        }
        stage("deploy production") {
            steps {
                // build docker images,push,run production container(port 18000)
                sh '''
            fun_Dockerfile(){
                    echo "FROM adoptopenjdk/openjdk11
                    RUN mkdir -p workspace
                    COPY build/libs/*.jar /workspace
                    EXPOSE 8080"
                    echo 'ENTRYPOINT ["java","-jar","/workspace/staff_management_microservice-0.0.1-SNAPSHOT.jar"]'
 }
            fun_Dockerfile > Dockerfile
                    echo $(docker build -t rathjr/staff_management_microservice_production .) > imagesLog_pro.txt
                    VR=$(grep 'Step 4'  imagesLog_pro.txt)
            if [ "$VR" == "" ]
            then
                    echo "images build failed"
            else
                    echo "build imaegs success"
                    docker login -u rathjr -p Rathkhmer2021
                    docker push rathjr/staff_management_microservice_production
                    docker rm -f staff_management_microservice_production
                    docker run -d --name staff_management_microservice_production -p 18000:8080 rathjr/staff_management_microservice_production
                    echo "done deploy production"
            fi
          '''
            }
        }
    }
}
```
## Config Github Webhook with jenkins

- go to project and click setting > webhook > add webhook

![Logo](https://i.ibb.co/dcvhVB8/Capture.png)

- pass url of jenkins server to payload url with /github-webhook/
- change conten type to application/json
- tick enable ssl verification
- tick just the push event

![Logo](https://i.ibb.co/0Yp6WXZ/Capture.png)
